const express = require("express");
const app = express();
app.use(express.json());
app.use(express.static("."));
app.listen(8080);

const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

// dang ky
app.post("/dangky", async (req, res) => {
  let { email, mat_khau, ho_ten, tuoi } = req.body;
  await prisma.nguoi_dung.create({
    data: { email, mat_khau, ho_ten, tuoi },
  });
});

// dang nhap
app.post("/dangnhap", async (req, res) => {
  let { email, mat_khau } = req.body;
  let data = await prisma.nguoi_dung.findFirst({
    where: {
      email,
      mat_khau,
    },
  });
  if (data) {
    res.send(data);
  } else {
    res.send("sai tk hoac mk");
  }
});

// chinh sua thong tin ca nhan
app.post("/capnhatthongtin", async (req, res) => {
  let { nguoi_dung_id, email, mat_khau, ho_ten, tuoi } = req.body;
  await prisma.nguoi_dung.update({
    data: { nguoi_dung_id, email, mat_khau, ho_ten, tuoi },
    where: { nguoi_dung_id },
  });
});

// lay danh sach anh ve
app.get("/laydanhsachanh", async (req, res) => {
  let data = await prisma.hinh_anh.findMany({
    include: {
      nguoi_dung: true,
    },
  });
  res.send(data);
});

// lay danh sach anh theo ten
app.get("/laydanhsachanhtheoten", async (req, res) => {
  let { tenanh } = req.query;
  let data = await prisma.hinh_anh.findMany({
    include: {
      nguoi_dung: true,
    },
    where: {
      ten_hinh: {
        contains: tenanh,
      },
    },
  });
  res.send(data);
});

// them 1 anh cua user
app.post("/themanh", async (req, res) => {
  let { ten_hinh, duong_dan, mo_ta, nguoi_dung_id } = req.body;
  await prisma.hinh_anh.create({
    data: { ten_hinh, duong_dan, mo_ta, nguoi_dung_id },
  });
});

// lay thong tin anh va nguoi tao anh
app.get("/laythongtinanhvanguoitao", async (req, res) => {
  let { hinh_id } = req.query;
  let data = await prisma.hinh_anh.findMany({
    include: {
      nguoi_dung: true,
    },
    where: {
      hinh_id: +hinh_id,
    },
  });
  res.send(data);
});

// lay thong tin binh luan
app.get("/laythongtinbinhluan", async (req, res) => {
  let { hinh_id } = req.query;
  let data = await prisma.binh_luan.findMany({
    include: {
      hinh_anh: true,
    },
    where: {
      hinh_id: +hinh_id,
    },
  });
  res.send(data);
});

// lay thong tin user
app.get("/laythongtinuser", async (req, res) => {
  let data = await prisma.nguoi_dung.findMany({});
  res.send(data);
});

// lay danh sach anh da luu
app.get("/laydanhsachanhdaluu", async (req, res) => {
  let { nguoi_dung_id } = req.query;
  let data = await prisma.nguoi_dung.findMany({
    include: {
      luu_anh: true,
    },
    where: {
      nguoi_dung_id: +nguoi_dung_id,
    },
  });
  res.send(data);
});

//  lay danh sach anh da tao theo id
app.get("/laydanhsachanhdatao", async (req, res) => {
  let { nguoi_dung_id } = req.query;
  let data = await prisma.nguoi_dung.findMany({
    include: {
      hinh_anh: true,
    },
    where: {
      nguoi_dung_id: +nguoi_dung_id,
    },
  });
  res.send(data);
});

// xoa anh da tao theo id
app.post("/xoaanhdatao", async (req, res) => {
  let { hinh_id } = req.query;
  await prisma.hinh_anh.delete({
    where: {
      hinh_id: +hinh_id,
    },
  });
});
